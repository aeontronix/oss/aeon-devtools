/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeondevtools.azure;

import com.aeontronix.aeoncli.AbstractRootCommand;
import com.aeontronix.aeondevtools.azure.vault.VaultCmd;
import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "azure",aliases = "az", subcommands = {
        VaultCmd.class
})
public class AzureCmd extends AbstractRootCommand {
    @CommandLine.Option(names = "-i")
    private String clientId;
    @CommandLine.Option(names = "-s")
    private String clientSecret;
    @CommandLine.Option(names = "-t")
    private String tenantId;
    private ClientSecretCredential credential;

    public ClientSecretCredential getCredentials() {
        if( credential == null ) {
            credential = new ClientSecretCredentialBuilder()
                    .clientId(clientId).clientSecret(clientSecret)
                    .tenantId(tenantId).build();
        }
        return credential;
    }
}
