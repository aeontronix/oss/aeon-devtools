/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeondevtools.azure.vault;

public class VaultAssetNotFoundException extends Exception {
    public VaultAssetNotFoundException() {
    }

    public VaultAssetNotFoundException(String message) {
        super(message);
    }

    public VaultAssetNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public VaultAssetNotFoundException(Throwable cause) {
        super(cause);
    }

    public VaultAssetNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
