/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeondevtools.azure.vault;

import com.aeontronix.aeondevtools.azure.AzureCmd;
import com.azure.core.exception.ResourceNotFoundException;
import com.azure.security.keyvault.certificates.CertificateClient;
import com.azure.security.keyvault.certificates.CertificateClientBuilder;
import com.azure.security.keyvault.certificates.models.KeyVaultCertificateWithPolicy;
import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;
import com.azure.security.keyvault.secrets.models.KeyVaultSecret;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import picocli.CommandLine.Command;
import picocli.CommandLine.ParentCommand;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Command(name = "vault", aliases = "v", description = "Vault commands", subcommands = GetVaultKeystoreCmd.class)
public class VaultCmd {
    @ParentCommand
    private AzureCmd azureCmd;
    private final Map<String, SecretClient> secretClients = new HashMap<>();
    private final Map<String, CertificateClient> certificateClients = new LinkedHashMap<>();

    private CertificateClient getCertificateClient(String vaultUri) {
        return new CertificateClientBuilder().vaultUrl(convertVaultUri(vaultUri)).credential(azureCmd.getCredentials()).buildClient();
    }

    @NotNull
    private synchronized CertificateClient getCertClient(@Nullable String vaultUri) {
        return certificateClients.computeIfAbsent(vaultUri, v ->
                new CertificateClientBuilder().vaultUrl(convertVaultUri(v)).credential(azureCmd.getCredentials()).buildClient());
    }

    @NotNull
    private synchronized SecretClient getSecretsClient(@Nullable String vaultUri) {
        return secretClients.computeIfAbsent(vaultUri, v ->
                new SecretClientBuilder().vaultUrl(convertVaultUri(v)).credential(azureCmd.getCredentials()).buildClient());
    }

    public String findSecret(String key) throws VaultAssetNotFoundException {
        return findSecret(null, key);
    }

    public String findSecret(String vaultUri, String key) throws VaultAssetNotFoundException {
        final KeyVaultSecret secret;
        try {
            secret = getSecretsClient(vaultUri).getSecret(key);
        } catch (ResourceNotFoundException e) {
            throw new VaultAssetNotFoundException("Couldn't find vault secret " + key + " : " + e.getMessage());
        }
        return secret.getValue();
    }

    public byte[] findCert(@NotNull String key) throws VaultAssetNotFoundException {
        return findCert(null, key);
    }

    public byte[] findCert(@Nullable String vaultUrl, @NotNull String key) throws VaultAssetNotFoundException {
        try {
            final KeyVaultCertificateWithPolicy certificate = getCertClient(vaultUrl).getCertificate(key);
            return certificate.getCer();
        } catch (ResourceNotFoundException e) {
            throw new VaultAssetNotFoundException("Couldn't find cert " + key + " : " + e.getMessage());
        }
    }

    public byte[] findKeystore(@NotNull String key, char[] keyPassword, char[] storePassword) throws VaultAssetNotFoundException {
        return findKeystore(null, key, keyPassword, storePassword);
    }

    public byte[] findKeystore(@Nullable String vaultUrl, @NotNull String key, char[] keyPassword, char[] storePassword) throws VaultAssetNotFoundException {
        try {
            final String v = getSecretsClient(vaultUrl).getSecret(key).getValue();
            final ByteArrayInputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(v));
            KeyStore p12Store = KeyStore.getInstance("PKCS12");
            p12Store.load(is, null);
            final byte[] cert = getCertClient(vaultUrl).getCertificate(key).getCer();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            final Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(cert));
            final String alias = p12Store.aliases().nextElement();
            final Key pkey = p12Store.getKey(alias, new char[0]);
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(null, null);
            keyStore.setKeyEntry("cert", pkey, keyPassword, new Certificate[]{certificate});
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            keyStore.store(os, storePassword);
            os.close();
            return os.toByteArray();
        } catch (ResourceNotFoundException e) {
            throw new VaultAssetNotFoundException("Couldn't find cert " + key + " : " + e.getMessage());
        } catch (UnrecoverableKeyException e) {
            throw new RuntimeException("Private key is not recoverable: " + e.getMessage(), e);
        } catch (CertificateException | KeyStoreException | NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @NotNull
    private String convertVaultUri(@NotNull String vaultUri) {
        if (!vaultUri.toLowerCase().startsWith("https://")) {
            return "https://" + vaultUri + ".vault.azure.net/";
        } else {
            return vaultUri;
        }
    }

}
