/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeondevtools.azure.vault;

import com.aeontronix.aeondevtools.azure.AzureCmd;
import org.slf4j.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.concurrent.Callable;

import static org.slf4j.LoggerFactory.getLogger;

@Command(name = "getkeystore",aliases = "gks",description = "Download a keystore")
public class GetVaultKeystoreCmd implements Callable<Integer> {
    private static final Logger logger = getLogger(GetVaultKeystoreCmd.class);
    @ParentCommand
    private VaultCmd vaultCmd;
    @Parameters(index = "0", description = "Vault name or URI")
    private String vaultName;
    @Parameters(index = "1", description = "Certificate name")
    private String certName;
    @Parameters(index = "2", description = "Key password")
    private String keyPw;
    @Parameters(index = "3", description = "Store password")
    private String storePw;
    @Parameters(index = "4", description = "File to save keystore as")
    private File file;

    @Override
    public Integer call() throws Exception {
        byte[] keystore = vaultCmd.findKeystore(vaultName, certName, keyPw.toCharArray(), storePw.toCharArray());
        try(FileOutputStream fs = new FileOutputStream(file)) {
            fs.write(keystore);
        }
        logger.info("Saved keystore as "+file.getPath());
        return 0;
    }
}
