/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeondevtools;

import com.aeontronix.aeoncli.AbstractRootCommand;
import com.aeontronix.aeoncli.CLILauncher;
import com.aeontronix.aeoncli.config.ProfileCmd;
import com.aeontronix.aeondevtools.azure.AzureCmd;
import picocli.CommandLine.Command;

@Command(name = "adt",subcommands = {
        ProfileCmd.class,
        AzureCmd.class
})
public class DevToolsCLI extends AbstractRootCommand {
    public static void main(String[] args) {
        new CLILauncher(new DevToolsCLI(), args).execute();
    }
}
